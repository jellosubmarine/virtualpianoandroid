/*
#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring

JNICALL
Java_com_example_john_virtualpiano_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject) {
    std::string hello = "Hello from C++";
    return env->NewStringUTF(hello.c_str());
}
*/
#include <assert.h>
#include <jni.h>
#include <pthread.h>

#include <android/native_window_jni.h>
#include "opencv2/objdetect.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdexcept>
#include <camera/NdkCameraDevice.h>
#include <cmath>
#include <vector>
#include <camera/NdkCameraManager.h>

#define localWidth 1280
#define localHeight 720
#define localChannels 4

using namespace std;
using namespace cv;

struct FINGER {
    int x;
    int y;
    bool key_pushed;
    int ID;
};

/*
static ANativeWindow *theNativeWindow;
static ACameraDevice *cameraDevice;
static ACaptureRequest *captureRequest;
static ACameraOutputTarget *cameraOutputTarget;
static ACaptureSessionOutput *sessionOutput;
static ACaptureSessionOutputContainer *captureSessionOutputContainer;
static ACameraCaptureSession *captureSession;

static ACameraDevice_StateCallbacks deviceStateCallbacks;
static ACameraCaptureSession_stateCallbacks captureSessionStateCallbacks;
*/

// Declare structure to be used to pass data from C++ to Mono.
//struct Circle
//{
//    Circle(int x, int y, int radius) : X(x), Y(y), Radius(radius) {}
//    int X, Y, Radius;
//};

//CascadeClassifier _faceCascade;
//String _windowName = "Unity OpenCV Interop Sample";f
//VideoCapture _capture;
//VideoCapture *cap;
//int _scale = 1;
static uchar result[localWidth*localHeight*localChannels];
//Vec3b lowerBound = (0, 225, 53), upperBound = (32, 255, 243);
Vec3b lowerBound = (0,0,0), upperBound = (255,255,255);
//THRESHOLDING Values
uint8_t THRESH_HUE = 1;
uint8_t THRESH_SAT = 1;
uint8_t THRESH_VAL = 1;


//extern "C" int __declspec(dllexport) __stdcall  Init(int& outCameraWidth, int& outCameraHeight)
//{
//	// Load LBP face cascade.
//	if (!_faceCascade.load("lbpcascade_frontalface.xml"))
//		return -1;
//
//	// Open the stream.
//	_capture.open(0);
//	if (!_capture.isOpened())
//		return -2;
//
//	outCameraWidth = _capture.get(CAP_PROP_FRAME_WIDTH);
//	outCameraHeight = _capture.get(CAP_PROP_FRAME_HEIGHT);
//
//	return 0;
//}
//
//extern "C" void Initialize(int& outCameraWidth, int& outCameraHeight) {
//    cap = new VideoCapture(CV_CAP_ANDROID_BACK);
//    outCameraWidth = (*cap).get(CAP_PROP_FRAME_WIDTH);
//    outCameraHeight = (*cap).get(CAP_PROP_FRAME_HEIGHT);
//}

//Finding the new lower bound for inrange masking
Vec3b NewThresholdsLow(Vec3b old_val, Vec3b new_val) {
    Vec3b low_bound = (0,0,0);
    if(new_val[0] < (old_val[0] + THRESH_HUE)) {
        if (new_val[0] - THRESH_HUE > 0) {
            low_bound[0] = uint8_t(new_val[0] - THRESH_HUE);
        }
        else {
            low_bound[0] = 0;
        }
    }
    else {
        low_bound[0] = old_val[0];
    }
    if (new_val[1] < (old_val[1] + THRESH_SAT)) {
        if (new_val[1] - THRESH_SAT > 0) {
            low_bound[1] = uint8_t(new_val[1] - THRESH_SAT);
        }
        else {
            low_bound[1] = 0;
        }
    }
    else {
        low_bound[1] = old_val[1];
    }
    if (new_val[2] < (old_val[2] + THRESH_VAL)) {
        if (new_val[2] - THRESH_VAL > 0) {
            low_bound[2] = uint8_t(new_val[2] - THRESH_VAL);
        }
        else {
            low_bound[2] = 0;
        }
    }
    else {
        low_bound[2] = old_val[2];
    }

    return low_bound;
}

//Finding the new upperbound for inrange masking
Vec3b NewThresholdsUp(Vec3b old_val, Vec3b new_val) {

    Vec3b upperBound = (0, 0, 0);
    if (new_val[0] > (old_val[0] - THRESH_HUE)) {
        if ((new_val[0] + THRESH_HUE) < 180) {
            upperBound[0] = uint8_t(new_val[0] + THRESH_HUE);
        }
        else {
            upperBound[0] = 179;
        }
    }
    else {
        upperBound[0] = old_val[0];
    }
    if (new_val[1] > (old_val[1] - THRESH_SAT)) {
        if ((new_val[1] + THRESH_SAT) < 256) {
            upperBound[1] = uint8_t(new_val[1] + THRESH_SAT);
        }
        else {
            upperBound[1] = 255;
        }
    }
    else {
        upperBound[1] = old_val[1];
    }
    if (new_val[2] > (old_val[2] - THRESH_VAL)) {
        if ((new_val[2] + THRESH_VAL) < 256) {
            upperBound[2] = uint8_t(new_val[2] + THRESH_VAL);
        }
        else {
            upperBound[2] = 255;
        }
    }
    else {
        upperBound[2] = old_val[2];
    }
    return upperBound;
}

extern "C" uchar* ReturnContours(uchar *img, FINGER *detected) {
    Mat byteImage = Mat(localHeight, localWidth, CV_8UC4, img);
    cvtColor(byteImage, byteImage, COLOR_RGBA2RGB);

    //GaussianBlur(byteImage, byteImage, Size(11, 11), 0, 0);
    vector<Mat> channels(3);


    cvtColor(byteImage, byteImage, COLOR_RGB2HSV);

    //split(byteImage, channels);
    ////Joosep's code
    //
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    Mat out;
    inRange(byteImage, lowerBound, upperBound, out);

    findContours(out, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);


    sort(contours.begin(), contours.end(), [](const vector<Point>& c1, const vector<Point>& c2) {
        return contourArea(c1, false) < contourArea(c2, false);
    });
    //byteImage = Mat::zeros(out.size(), CV_8UC3);

    for (int i = 0; i < contours.size(); i++) {
        drawContours(byteImage, contours, i, Scalar(255, 255, 255), 2, 8);
    }
    vector<vector<Point> > big_cont;
    int num_fingers = 10;
    for (int i = 0; i < contours.size(); i++) {
        if(contourArea(contours[contours.size()-i-1]) > 100)
            big_cont.push_back(contours[contours.size()-i-1]);
    }
    contours = big_cont;
    num_fingers = MIN(num_fingers, contours.size());
    for (int i = 0; i < 10; i++) {
        if (i < num_fingers) {
            Moments M = moments(contours[i], false);
            Point P = Point(round(M.m10 / M.m00), round(M.m01 / M.m00));
            detected[i].x = P.x;
            detected[i].y = P.y;
            detected[i].key_pushed = true;
            detected[i].ID = i;
        }
        else {
            detected[i].x = 0;
            detected[i].y = 0;
            detected[i].key_pushed = false;
            detected[i].ID = i;
        }
    }

    /// Draw contours
    for (int i = 0; i < num_fingers; i++) {
        drawContours(byteImage, contours, i, Scalar(255,0, 0 ), 2, 8);
    }

    //Mat mask;
    Mat mask = Mat(localHeight, localWidth, CV_8UC1, cv::Scalar(125));
    split(byteImage, channels);
    channels.push_back(mask);
    merge(channels, byteImage);


    memcpy(result, byteImage.data, byteImage.cols*byteImage.rows * localChannels);

    return result;
}


extern "C" uchar* ProcessFrame(uchar *img) {
    Mat byteImage = Mat(localHeight, localWidth, CV_8UC4, img);
    cvtColor(byteImage, byteImage, COLOR_RGBA2RGB);

    //GaussianBlur(byteImage, byteImage, Size(11, 11), 0, 0);
    vector<Mat> channels(3);


    cvtColor(byteImage, byteImage, COLOR_RGB2HSV);

    split(byteImage, channels);
    ////Joosep's code
    //
    ////Thresholding color for first run


    //Mat mask;
    Mat mask = Mat(localHeight, localWidth, CV_8UC1, cv::Scalar(105));
    Mat mask1;
    inRange(byteImage, lowerBound, upperBound, mask1);
    Mat out = Mat(localHeight, localWidth, CV_8UC1, cv::Scalar(0));
    mask.copyTo(out, mask1);
    channels.push_back(out);
    merge(channels, byteImage);


    memcpy(result, byteImage.data, byteImage.cols*byteImage.rows * localChannels);

    return result;
}

extern "C" void FirstCalibrate(uchar *img) {
    Mat byteImage = Mat(localHeight, localWidth, CV_8UC4, img);
    cvtColor(byteImage, byteImage, COLOR_RGBA2RGB);
    cvtColor(byteImage, byteImage, COLOR_RGB2HSV);
    Vec3b new_color = byteImage.at<Vec3b>(localHeight/2, localWidth/2);
    lowerBound[0] = uint8_t(abs(new_color[0] - 5));
    lowerBound[1] = uint8_t(abs(new_color[1] - 5));
    lowerBound[2] = uint8_t(abs(new_color[2] - 10));
    if (new_color[0] + 5 < 180) {
        upperBound[0] = uint8_t(new_color[0] + 5);
    }
    else {
        upperBound[0] = 179;
    }
    if (new_color[1] + 5 < 256) {
        upperBound[1] = uint8_t(new_color[1] + 5);
    }
    else {
        upperBound[1] = 255;
    }
    if (new_color[2] + 10 < 256) {
        upperBound[2] = uint8_t(new_color[2] + 10);
    }
    else {
        upperBound[2] = 255;
    }
}

extern "C" void Calibrate(uchar *img) {
    Mat byteImage = Mat(localHeight, localWidth, CV_8UC4, img);
    cvtColor(byteImage, byteImage, COLOR_RGBA2RGB);
    cvtColor(byteImage, byteImage, COLOR_RGB2HSV);
    Vec3b new_color = byteImage.at<Vec3b>(localHeight/2, localWidth/2);
    lowerBound = NewThresholdsLow(lowerBound, new_color);
    upperBound = NewThresholdsUp(upperBound, new_color);
}

extern "C" void ResetThreshold() {
    lowerBound = (0, 0, 0);
    upperBound = (255,255,255);
}


//
//extern "C" void Close()
//{
//    (*cap).release();
//}
//
//extern "C" int Five()
//{
//    return 5;
//}

//extern "C" void SetScale(int scale)
//{
//    _scale = scale;
//}



//extern "C" void __declspec(dllexport) __stdcall Detect(Circle* outFaces, int maxOutFacesCount, int& outDetectedFacesCount)
//{
//	Mat frame;
//	_capture >> frame;
//	if (frame.empty())
//		return;
//
//	std::vector<Rect> faces;
//	// Convert the frame to grayscale for cascade detection.
//	Mat grayscaleFrame;
//	cvtColor(frame, grayscaleFrame, COLOR_BGR2GRAY);
//	Mat resizedGray;
//	// Scale down for better performance.
//	resize(grayscaleFrame, resizedGray, Size(frame.cols / _scale, frame.rows / _scale));
//	equalizeHist(resizedGray, resizedGray);
//
//	// Detect faces.
//	_faceCascade.detectMultiScale(resizedGray, faces);
//
//	// Draw faces.
//	for (size_t i = 0; i < faces.size(); i++)
//	{
//		Point center(_scale * (faces[i].x + faces[i].width / 2), _scale * (faces[i].y + faces[i].height / 2));
//		ellipse(frame, center, Size(_scale * faces[i].width / 2, _scale * faces[i].height / 2), 0, 0, 360, Scalar(0, 0, 255), 4, 8, 0);
//
//		// Send to application.
//		outFaces[i] = Circle(faces[i].x, faces[i].y, faces[i].width / 2);
//		outDetectedFacesCount++;
//
//		if (outDetectedFacesCount == maxOutFacesCount)
//			break;
//	}
//
//	// Display debug output.
//	imshow(_windowName, frame);
//}